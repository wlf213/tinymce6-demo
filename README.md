# tinymce6-demo

## 介绍
最新的tinymce6富文本编辑器，基础集成样例。包括本地化，图片上传等一些配置，拿来即用，无需过多关注一些细节。

## 目录说明

- `vanillajs`目录 ---原生JS实现
- 文件名即意思

## 使用说明

克隆项目，使用VsCode分别打开二级目录，

- `vanillajs`目录，需要安装`ritwickdey.LiveServer`插件。打开`demo.html`文件，右键`open with LiveServer`
- 有打包工具的情况下，查看统一执行`npm run dev`命令，根据提示输出，浏览器打开对应网址即可。



